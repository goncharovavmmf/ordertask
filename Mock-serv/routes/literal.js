const express = require('express');
const router = express.Router();
const path = require('path');
var faker = require('faker');
/* GET home page. */
router.get('/', (req, res) => {
	console.log("look"+req.url);
	switch(req.url){
		case '/':{
			res.render('literal', {
			prepText: faker.lorem.paragraph() ,
			cookText: faker.lorem.paragraph(),
			servsText:faker.lorem.paragraph(),
			dish:faker.lorem.word(),
			prepTime:faker.finance.amount(1,100,0),
			cookTime:faker.finance.amount(1,100,0),
			servsTime:faker.finance.amount(1,100,0)});
		break;
		}
		case '/order.css' :{
			res.sendFile(path.resolve('public/css/order.css'));
		break;
		}
}
});
router.get('/mainDish', (req, res) => {
  res.send({
			"name": faker.lorem.word(),
			"id": "3001",
			"remTime": "3000"
		});
});
router.get('/drink', (req, res) => {
  res.send({
			"name": faker.lorem.word(),
			"id": "302",
			"remTime": "30"
		});
});
router.get('/desert', (req, res) => {
  res.send({
			"name": faker.lorem.word(),
			"id": "302",
			"remTime": "30"
		});
});
module.exports = router;
